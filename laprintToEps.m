function [ output_args ] = laprintToEps( figno, filename, varargin)
    
    cur_pwd = pwd;

    tmp_laprint = tempname;
    tmp_latex = [tempname '.tex'];
    [tmp_path,tmp_latex_filename,ext]=fileparts(tmp_latex);
    laprint(figno, tmp_laprint, varargin{:});
    
    fid = fopen(tmp_latex, 'w');
    fwrite(fid, ['\documentclass{article} \usepackage{graphicx,color,psfrag} \pagestyle{empty} \begin{document} \begin{center} \input{' basename(tmp_laprint) '} \end{center} \end{document}']);
    fclose(fid);
    
    cd(tmp_path);
    system('pwd');
    system(['latex --src -interaction=nonstopmode "' basename(tmp_latex) '"']);
    system(['dvips "' tmp_latex_filename '.dvi"']);
    system(['ps2eps.pl "' tmp_latex_filename '.ps"']);
    
    [destDir, destFile, destExt] = fileparts(filename);
    if strcmp(destDir, '') == 1
        destDir = cur_pwd;
    end;
    if strcmp(destExt, '') == 1
        destExt = '.eps'
    end;
    
    copyfile([tmp_latex_filename '.eps'], [destDir '/' destFile destExt]);
    cd(cur_pwd);

    
    function output = basename(theFile)
        %BASENAME   Filename component of path
        %   If the input is a folder, the last subfolder is returned
        %
        %   Syntax:
        %      OUT = BASENAME(FILEPATH)
        %
        %   Input:
        %      FILEPATH   Path to file or folder
        %
        %   Output:
        %      Filename component of path if using the path to a file or the
        %      last subfolder when using the path to a folder
        %
        %   Example:
        %      basename('../myfolder')                    % returns myfolder
        %      basename('/home/user/myfolder')            % returns myfolder
        %      basename('/home/user/myfolder/myfile.txt') % returns myfile.txt
        %
        %   MMA 18-09-2005, martinho@fis.ua.pt
        %
        %   See also DIRNAME, REALPATH

        %   Department of Physics
        %   University of Aveiro, Portugal

        %d = realpath(theFile);
        %[path,name,ext]=fileparts(d);
        [path,name,ext]=fileparts(theFile);
        output = [name,ext];
    end



end

