# laprintToEps

A wrapper function for the popular `MATLAB` function [`LaPrint`][1] that scripts and automates the following tasks:

- dumps laprint output files to temporary folder
- creates temporary `LaTeX` that is necessary to include the newly created laprint graphic
- calls `LaTeX` and `dvips` to render a postscript file
  with LaPrint annotations
- uses `ps2eps.pl` by Roland Bless to create the final EPS graphic


# Requirements
 - [`Laprint`][1]
 - `LaTeX`
 - [`eps2ps.pl`][2], a perl script from Roland Bless
 - `latex`, `dvips` and `eps2ps.pl` must be in your $PATH variable
 - Tested on Windows 7 and Ubuntu 12.04


# Usage

	fig = figure
	plot(x, y)
	xlabel('X-axis')
	ylabel('Y-axis')
	laprintToEps(fig, '/home/user/myimage.eps')

# Additional Parameters

All additional parameters are passed to Laprint:

	laprintToEps(fig, '/home/user/myimage.eps', 'width', 10 , 'factor', 1.0, 'scalefonts', 'on')



[1]: http://www.uni-kassel.de/fb16/rat/matlab/laprint/
[2]: http://www.tm.uka.de/~bless/ps2eps
